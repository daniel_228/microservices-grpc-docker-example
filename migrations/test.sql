create table test
  (
     id        serial primary key,
     msg       text not null,
     timestamp timestamptz default now() not null
  ); 
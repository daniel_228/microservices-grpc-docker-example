gen-proto:
	-mkdir proto
	protoc $(wildcard api/*/*.proto) \
	--proto_path=. \
	--go_out=./proto \
	--go_opt=paths=source_relative \
	--go-grpc_out=./proto \
	--go-grpc_opt=paths=source_relative \
	--experimental_allow_proto3_optional 
package main

import (
	"context"
	"fmt"
	"net"
	"os"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/timestamppb"

	proto_b "service_b/proto/api/b"
)

type Handler struct {
	proto_b.UnimplementedBServer
}

func (h *Handler) Ping(_ context.Context, req *proto_b.PingRequest) (*proto_b.PingResponse, error) {
	fmt.Println("Ping() request")
	defer fmt.Println("done successfully")

	return &proto_b.PingResponse{
		Pong:      "pong",
		Timestamp: timestamppb.New(time.Now()),
	}, nil
}

func main() {
	lst, err := net.Listen("tcp", ":"+os.Getenv("PORT"))
	if err != nil {
		panic(err)
	}

	srv := grpc.NewServer()

	proto_b.RegisterBServer(srv, &Handler{})

	if err := srv.Serve(lst); err != nil {
		fmt.Println(err)
	}
}

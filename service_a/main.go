package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/TwiN/go-color"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	proto_b "service_a/proto/api/b"
)

const DBConnRetries = 10

func main() {
	var db *sqlx.DB

	period := 16 * time.Millisecond

	ticker := time.NewTicker(period)
	defer ticker.Stop()

	for i := 0; i < DBConnRetries; i++ {
		<-ticker.C
		fmt.Printf("Attempt %d: connecting to DB\n", i+1)

		var err error

		db, err = sqlx.Connect("postgres", os.Getenv("DB_URL"))
		if err != nil {
			fmt.Printf("Attempt %d failed: DB error: %v\n", i+1, err)

			if i+1 < DBConnRetries {
				if period < 5*time.Second {
					period *= 2
				} else {
					period = 10 * time.Second
				}

				ticker.Reset(period)
			} else {
				fmt.Printf("Can't connect to DB: %v\n", err)
				return
			}
		} else {
			fmt.Println("Successfully connected to DB!")
			break
		}
	}

	conn, err := grpc.DialContext(
		context.Background(),
		os.Getenv("B_URL"),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock(),
	)
	if err != nil {
		panic(err)
	}

	cl := proto_b.NewBClient(conn)

	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("/ping request")

		resp, err := cl.Ping(context.Background(), &proto_b.PingRequest{})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			fmt.Println(color.InRed(err))
			return
		}

		_, err = db.Exec("INSERT INTO test (msg, \"timestamp\") VALUES ($1, $2)", resp.Pong, resp.Timestamp.AsTime())
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			fmt.Println(color.InRed(err))
			return
		}

		w.Write([]byte(resp.Pong))
		fmt.Println("done successfully")
	})

	err = http.ListenAndServe(":"+os.Getenv("PORT"), nil)
	if err != nil {
		fmt.Println(err)
	}
}
